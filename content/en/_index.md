---
title: "Floating Blue"

description: "An initiative involving open source, play,and earth observation"
cascade:
  featured_image: '/images/floating-blue_medres.png'
---

**SAVE THE DATE!**

When: Thursday 27 February to Sunday 2 March 2025

Where: The Expolab, Lagoa, Ilha de São Miguel, Açores, as well as online in a variety of formats, even pre-recorded.

Why: This event, and hopefully extending into future iniatives, is a playful way to explore how to bring physical world elements into casual games. 

This initiative also aligns with the main organiser's research, focused on exploring the transformation of expertise in the context of socio-metabolism (as an example of regulation science, which has been prone to expertise contestation) as imagined in the Metaverse, or post-digital context, using game design.

Language: English and Portuguese, but any language is welcome, we make a translation plan.

Coming soon: supporters, topics, invited speakers... 

Keen to get involved? Contact indiebio on whatever platform you prefer, or email bernellev at gmail dot com



This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
and [Hugo](https://gohugo.io), and can be built in under 1 minute.
Literally. 
Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
